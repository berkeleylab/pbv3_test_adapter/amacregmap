#!/usr/bin/env python
# %%
import openpyxl
import jinja2

# %% Prepare output template
loader = jinja2.FileSystemLoader(searchpath="./data")
env = jinja2.Environment(loader=loader)
template = env.get_template('AMACv2RegMap_template.cpp')

# %%
wb = openpyxl.load_workbook(filename='data/AMACstar.xlsx', data_only=True)

# Get All Sheets
a_sheet_names = wb.get_sheet_names()
print(a_sheet_names)

# Get Sheet Object by names
o_sheet = wb.get_sheet_by_name("AMACStar Registers")
print(o_sheet)

# %%
fields=[]
registers=[]
for i in range(o_sheet.max_row):
    regaddr=o_sheet.cell(row=i+1, column=1).value
    if regaddr==None: continue # this is not a register
    try:
        regaddr=int(o_sheet.cell(row=i+1, column=4).value)
    except ValueError:
        continue # not a register address

    regname=o_sheet.cell(row=i+1, column=2).value
    rw=o_sheet.cell(row=i+1, column=38).value
    print(regaddr,regname,rw)
    registers.append({'name':regname,'address':regaddr,'rw':rw})

    default=o_sheet.cell(row=i+1, column=37).value
    if default is None:
        default=0
    elif type(default) is str:
        default=int(default,16)

    # Fields in register
    for bitpos in range(0,32):
        c=o_sheet.cell(row=i+1, column=5+bitpos)

        bitval=c.value
        if bitval!=None: # Detected a new field
            field={'name':bitval,'register':regname,'width':1,'offset':31-bitpos,'default':0}
            fields.append(field)
        elif type(c)==openpyxl.cell.cell.MergedCell: # Add bit to the last field
            fields[-1]['width' ]+=1
            fields[-1]['offset']-=1
    for field in fields:
        field['default']=(default>>field['offset'])&(2**field['width']-1)
        print('\t',field)

# %%
outputText = template.render(registers=registers,fields=fields)  # this is where to put args to the template renderer
print(outputText)

fh=open('AMACv2RegMap_star.cpp','w')
fh.write(outputText)
fh.close()
# %%
