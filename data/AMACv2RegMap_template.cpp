#include "AMACv2RegMap.h"

void AMACv2RegMap::init_star()
{
    /** \name Registers
     * @{ */
    {% for register in registers -%}
    Register{{register.name}} = {"{{register.name}}", {{register.address}}, {{register.rw}}};
    {% endfor -%}

    /** @} */

    /** \name Fields
     * @{ */
    {% for field in fields -%}
    {{field.name}} = {"{{field.name}}", &Register{{field.register}}, {{field.offset}}, {{field.width}}, 0x{{'%x'%field.default}} };
    {% endfor -%}

    /** @} */
}